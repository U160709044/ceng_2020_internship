from b2m_tuner.metrics import BinaryMetrics, MulticlassMetrics
from b2m_tuner.models.model_io import PickleModelIO
from b2m_tuner.models.batch.batch_model_base import *

import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC


class LogisticClassifier(PickleModelIO, BatchLinearModel):
    
    @property
    def model_signature(self):
        return LogisticRegression #class_weight=balanced for balanced sets.
    
    @property
    def model_parameters(self):
        model_params = {"solver": "saga",
                        "max_iter": 1500}
        return model_params

    @classmethod
    def learning_space_distributions(self):
        return {"C": hp.uniform("C", 0.3, 2)}

    def get_current_loss(self):
        return {}

    def _setup(self, config):
        super()._setup(config)
        self.classes = np.unique(self.Y_train)
        if len(self.classes) > 2:
            self.metric = BinaryMetrics
        else:
            self.metric = MulticlassMetrics

class ElasticNetClassifier(LogisticClassifier):
    
    @property
    def model_parameters(self):
        model_params = {"solver": "saga",
                        "penalty": "elasticnet", # add penalty
                        "max_iter": 1500}
        return model_params

    @classmethod
    def learning_space_distributions(self):
        return {"l1_ratio": hp.uniform("l1_ratio", 0.2, 1), #add l1_ratio
                "C": hp.uniform("C", 0.3, 2)}
    
class LassoClassifier(LogisticClassifier):

    @property
    def model_parameters(self):
        model_params = {"solver": "saga",
                        "penalty": "l1",
                        "max_iter": 1500} # add penalty
        return model_params

class ANNClassifier(PickleModelIO, BatchANNModel):

    @property
    def model_signature(self):
        return MLPClassifier

    def _setup(self, config):
        super()._setup(config)
        self.classes = np.unique(self.Y_train)
        if len(self.classes) > 2:
            self.metric = BinaryMetrics
        else:
            self.metric = MulticlassMetrics

class LinearSVMClassifier(PickleModelIO):

    @property
    def model_signature(self):
        return MLPClassifier

class BaseLinearSVMClassifier(PickleModelIO, BatchSVMModel):

    @property
    def model_signature(self):
        return LinearSVC

    @property
    def model_parameters(self):
        return {"class_weight":"balanced"}

    @classmethod
    def learning_space_distributions(cls):
        #TODO DYNAMIC ARRANGEMENT OF PARAMETERS WHEN PENALTY IS L1
        return {**super().learning_space_distributions(),
                "penalty": hp.choice("penalty", ["l2"]), } #add l1_ratio
        # #dual should be automatically false , when n_samples > n_features.

    def _setup(self, config):
        super()._setup(config)
        self.classes = np.unique(self.Y_train)
        if len(self.classes) > 2:
            self.metric = BinaryMetrics
        else:
            self.metric = MulticlassMetrics

class BaseNuSVMClassifier(PickleModelIO, BatchSVMModel):

    @property
    def model_signature(self):
        return NuSVC

    @property
    def model_parameters(self):
        return {"class_weight":"balanced"}

    @classmethod
    def learning_space_distributions(cls):
        return {"nu": hp.uniform("nu", 0.3, 0.8),
                "kernel": hp.choice("kernel", ["linear","rbf"]),
                } #dual should be automatically false , when n_samples > n_features.

    def _setup(self, config):
        super()._setup(config)
        self.classes = np.unique(self.Y_train)
        if len(self.classes) > 2:
            self.metric = BinaryMetrics
        else:
            self.metric = MulticlassMetrics

class LargeSVMClassifier:
    pass

class LargeNuSVMClassifier:
    pass #Nyöstrem transformer + LinSVC