from b2m_tuner.metrics import RegressionMetrics
from b2m_tuner.models.model_io import PickleModelIO
from b2m_tuner.models.batch.batch_model_base import *

from sklearn.svm import LinearSVR, NuSVR
from sklearn.linear_model import Ridge, ElasticNet, Lasso


class LinearRegressor(PickleModelIO, BatchLinearModel):
    
    @property
    def model_signature(self):
        return Ridge #class_weight=balanced for balanced sets.
    
    @property
    def model_parameters(self):
        return {}

    @classmethod
    def learning_space_distributions(self):
        return {"alpha": hp.loguniform("alpha", -2, 2)}

    def _setup(self, config):
        super()._setup(config)
        self.metric = RegressionMetrics

    def get_current_loss(self):
        return {}

class ElasticNetRegressor(LinearRegressor):
    
    @property
    def model_signature(self):
        return ElasticNet    

    @classmethod
    def learning_space_distributions(self):
        return {"alpha": hp.loguniform("alpha", -2, 2), #add l1_ratio
                "l1_ratio": hp.uniform("l1_ratio", 0.3, 1)}
    
class LassoRegressor(LinearRegressor):
    
    @property
    def model_signature(self):
        return Lasso

class BaseLinearSVRRegressor(PickleModelIO, BatchSVMModel):

    @property
    def model_signature(self):
        return LinearSVR

    @classmethod
    def learning_space_distributions(cls):
        #TODO DYNAMIC ARRANGEMENT OF PARAMETERS WHEN PENALTY IS L1
        return {**super().learning_space_distributions(),
                "loss": hp.choice("loss", ["epsilon_insensitive", "squared_epsilon_insensitive"]),}
        #add l1_ratio
        # #dual should be automatically false , when n_samples > n_features.

    def _setup(self, config):
        super()._setup(config)
        self.metric = RegressionMetrics

class BaseNuSVRRegressor(PickleModelIO, BatchSVMModel):

    @property
    def model_signature(self):
        return NuSVR

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "nu": hp.uniform("nu", 0.3, 0.8),
                "kernel": hp.choice("kernel", ["linear","rbf"]),}
        #dual should be automatically false , when n_samples > n_features.

    def _setup(self, config):
        super()._setup(config)
        self.metric = RegressionMetrics
