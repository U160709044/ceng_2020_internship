from b2m_tuner.models.model_base import *
from b2m_tuner.optimization_base import BatchParameterTuner

class BatchLinearModel(LinearModel, BatchParameterTuner):

    @property
    def model_parameters(self):
        return {**super(LinearModel, self).model_parameters,
                **{"early_stopping": True,
                   "max_iter":5000}}


class BatchANNModel(ANNModel, BatchParameterTuner):

    @property
    def model_parameters(self):
        return {**super(BatchANNModel, self).model_parameters,
                **{"early_stopping": True,}}


class BatchSVMModel(SVMModel, BatchParameterTuner):

    def get_current_loss(self):
        return {}

class BatchAutoMLModel:
    pass