import os
import pickle
import lightgbm as lgb

#TODO MAKE MODEL NAMES NOT DUPLICATED.
class PickleModelIO:

    def _save(self, checkpoint_dir):
        model_name = "linregress"
        model_path = os.path.join(checkpoint_dir, model_name)
        with open(model_path, "wb") as output_path:
            pickle.dump(self.model, output_path)
        return model_path

    def _restore(self, checkpoint_dir):
        with open(checkpoint_dir, "rb") as model_file:
            self.model = pickle.load(model_file)

class BoosterModelIO:

    def _save(self, checkpoint_dir):
        model_name = "xgb.bin"
        model_path = os.path.join(checkpoint_dir, model_name)
        self.model.save_model(model_path)
        return model_path

    def _restore(self, checkpoint_dir):
        self.booster = self.model.load_model(checkpoint_dir)

class LigthGBMBoosterModelIO:

    def _save(self, checkpoint_dir):
        model_name = "lightgbm.bin"
        model_path = os.path.join(checkpoint_dir, model_name)
        self.booster.save_model(filename=model_path)
        return model_path

    def _restore(self, checkpoint_dir):
        self.booster = lgb.Booster(model_file=checkpoint_dir)

class TorchModelIO:
    pass

class TFModelIO:
    pass

class AutoMLIO:
    pass