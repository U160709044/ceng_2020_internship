from abc import ABC
from functools import partial

from b2m_tuner.models.incremental.incremental_model_base import PartialLinearModel
from b2m_tuner.utils.optimization_utils import CaptureSklearnReport

from sklearn.linear_model import SGDRegressor
from sklearn.neural_network import MLPClassifier
from numpy import unique

class PartialLinearMixin(PartialLinearModel):

    def train_model(self):
        with CaptureSklearnReport() as SklearnOut:
            self.partial_fit()(self.X_train, self.Y_train)

        self.skout = SklearnOut
        progress_metrics = self.evaulate_model(self.tracking_metrics)
        progress_metrics.update({"train_loss": self.get_current_loss})
        return progress_metrics

    def _setup(self, config):
        super()._setup(config)
        self.classes = unique(self.Y_train)

class PartiallANNClassifierMixin(PartialLinearMixin):

    @property
    def model_signature(self):
        return MLPClassifier

    def partial_fit(self):
        return partial(self.model.partial_fit, classes=self.classes)

class PartialLinearClassifierMixin(PartialLinearMixin):

    @property
    def model_signature(self):
        return SGDRegressor

    def partial_fit(self):
        return partial(self.model.partial_fit)