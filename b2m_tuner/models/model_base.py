from b2m_tuner.optimization_base import ModelBase

from hyperopt import hp

class LinearModel(ModelBase):

    @property
    def model_parameters(self):
        return {"verbose":1,
                "learning_rate": "invscaling",
                "penalty": "l2",}

    @classmethod
    def learning_space_distributions(cls):
        return {"eta0": hp.loguniform("eta0", -4, -1),
                "power_t": hp.uniform("power_t", 0.2, 0.6),
                "alpha": hp.loguniform("alpha", -6, -2), }

class LogisticClassificationModel(LinearModel):
    pass

class LassoClassificationModel(LinearModel):

    @property
    def model_parameters(self):
        return {**super(LassoClassificationModel, self).model_parameters,
                "penalty": "l1",}

class ElasticClassificationModel(LinearModel):

    @property
    def model_parameters(self):
        return {**super(ElasticClassificationModel, self).model_parameters,
                "penalty": "elasticnet",}

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "l1_ratio": hp.uniform("l1_ratio", 0.2, 1)}

class XGBBoosterModel(ModelBase):

    @property
    def model_parameters(self):
        return {'booster': 'gbtree',
                'tree_method': 'hist',
                'max_bin': 256,
                'grow_policy': 'depthwise',
                "n_estimators":1,}

    @classmethod
    def learning_space_distributions(cls):
        return {"learning_rate": 10 ** hp.uniform("learning_rate", -2, -0.5),
                "reg_lambda": 10 ** hp.uniform("reg_lambda", -1, 0.5),
                "reg_alpha": 10 ** hp.uniform("reg_alpha", -0.5, 0.5),
                "max_depth": hp.choice("max_depth", list(range(3, 11))),
                "subsample": hp.choice("subsample", [0.4, 0.55, 0.65, 0.75]),
                "colsample_bytree": hp.choice("colsample_bytree", [0.6, 0.7, 0.8]),
                "colsample_bylevel": hp.choice("colsample_bylevel", [0.7, 0.8, 0.9]),
                "colsample_bynode": hp.choice("colsample_bynode", [0.7, 0.85, 0.95]),
                "min_child_weight": hp.choice("min_child_weight", list(range(3,8))),}

class XGBoostDartModel(XGBBoosterModel):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "booster": "dart",}

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "sample_type": hp.choice("sample_type", ["uniform", "weighted"]),
                "normalize_type": hp.choice("normalize_type", ["tree", "forest"]),
                "rate_drop": hp.choice("rate_drop", [0.2, 0.3, 0.4]),
                "skip_drop": hp.choice("skip_drop", [0.2, 0.35, 0.5]),}


class XGBoostLinearModel(XGBBoosterModel):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "booster": "gblinear",}

    @classmethod
    def learning_space_distributions(cls):
        return {**{param:val for param, val in super().learning_space_distributions().items() \
                   if param not in ["colsample_bylevel", "colsample_bynode",
                                    "colsample_bytree", "grow_policy",
                                    "max_bin", "max_depth",
                                    "min_child_weight", "subsample", "tree_method"]},
                   "feature_selector": hp.choice("feature_selector",
                                              ["cyclic","shuffle"])}


class XGBBoostedRandomForestModel(XGBBoosterModel):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "num_parallel_tree": 1,
                "learning_rate": 1, }

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "n_estimators": hp.choice("n_estimators", list(range(10, 101)))}

class XGBoostedDartRandomForestModel(XGBoostDartModel):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "num_parallel_tree": 1,
                "learning_rate": 1,}

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "n_estimators": hp.choice("n_estimators", list(range(10, 101)))}

class XGBoostedLinearRandomForestModel(XGBoostLinearModel):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "num_parallel_tree": 1,
                "learning_rate": 1,}

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "n_estimators": hp.choice("n_estimators", list(range(10, 101)))}

class LightGBMModel(ModelBase):

    @property
    def model_parameters(self):
        return {'verbose': 1}

    @property
    def model_signature(self):
        return {}

    @classmethod
    def learning_space_distributions(cls):
        return {"learning_rate": 10 ** hp.uniform("learning_rate", -2, 0.5),
                "lambda_l2": 10 ** hp.uniform("lambda_l2", -1, 0.5),
                "lambda_l1": 10 ** hp.uniform("lambda_l1", -0.5, 0.5),
                "min_child_weight": 10 ** hp.uniform("min_child_weight", -4, 0),
                "num_leaves": hp.choice("num_leaves", list(range(15, 50, 3))),
                "max_depth": hp.choice("max_depth", list(range(3, 11))),
                "min_child_samples": hp.choice("min_child_samples", list(range(17, 32))),
                "subsample": hp.choice("subsample", [0.4, 0.55, 0.65, 0.75]),
                "subsample_freq": hp.choice("subsample_freq", [1, 3, 5]),
                "colsample_bytree": hp.choice("colsample_bytree", [0.6, 0.7, 0.8]),
                "colsample_bynode": hp.choice("colsample_bynode", [0.7, 0.85, 0.95]),
                "s": hp.choice("s", [True, False]),}

    @property
    def fit_method_parameters(self):
        return {"train_set":self.dataset,
                "num_boost_round":1,
                "valid_sets":[self.dataset],
                "valid_names":["train"],
                "keep_training_booster":True,
                "evals_result":self.eval_results}

    def evaulate_model(self, tracking_metrics):
        metrics = {}
        if hasattr(self, "X_validation"):
            metrics.update({"validation_" + metric: self.metric.get_metric_score(self.Y_validation,
                                                                                 self.booster.predict(self.X_validation),
                                                                                 metric)
                            for metric in tracking_metrics})

        metrics.update({"test_" + metric: self.metric.get_metric_score(self.Y_test,
                                                                       self.booster.predict(self.X_test),
                                                                       metric)
                        for metric in tracking_metrics})
        return metrics


class LightGBMDart(LightGBMModel):

    @property
    def model_parameters(self):
        return {**super(LightGBMDart, self).model_parameters,
                "boosting": "dart",}

    @classmethod
    def learning_space_distributions(cls):
        return {**super().learning_space_distributions(),
                "drop_rate": hp.choice("drop_rate", [0.2, 0.3, 0.4]),
                "skip_drop": hp.choice("skip_drop", [0.2, 0.35, 0.5]),
                "uniform_drop": hp.choice("uniform_drop", [False, True]),}

class LightGBMOneSideSampler(LightGBMModel):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "boosting": "goss",}

    @classmethod
    def learning_space_distributions(cls):
        return {**{param: value for param, value \
                  in super().learning_space_distributions().items() \
                  if param not in ["subsample"]},
                "top_rate": hp.choice("top_rate", [0.1, 0.17, 0.25, 0.38]),
                "other_rate": hp.choice("other_rate", [0.1, 0.17, 0.25, 0.38]),
                "uniform_drop": hp.choice("uniform_drop", [False, True]),}

class LightGBMRandomForest(LightGBMModel):

    @property
    def model_parameters(self):
        return {**super(LightGBMDart, self).model_parameters,
                "boosting": "rf",}


class ANNModel(ModelBase):

    @property
    def model_parameters(self):
        return {"activation": "relu",
                "solver": "sgd",
                "learning_rate": "invscaling",
                "early_stopping": False, }

    @property
    def get_current_loss(self):
        return self.model.loss_curve_.pop()

    @classmethod
    def learning_space_distributions(self):
        return {"learning_rate_init": hp.loguniform("learning_rate_init", -8, -2),
                "alpha": hp.loguniform("alpha", -7, -1),
                "power_t": hp.uniform("power_t", 0.3, 0.6),
                "max_iter": 1500}

class SVMModel(ModelBase):

    @classmethod
    def learning_space_distributions(cls):
        return {"C": hp.uniform("C", 0.5, 1.5)}


