from b2m_tuner.metrics import BinaryMetrics, MulticlassMetrics
from b2m_tuner.models.incremental.incremental_model_base import PartialXGBoost,\
                                                                            PartialLightGBM
from b2m_tuner.models.classifier_model_mixins import PartiallANNClassifierMixin, \
    PartialLinearClassifierMixin

from xgboost import XGBClassifier, XGBRFClassifier
from numpy import unique

class PartialLinearBinaryMixin(PartialLinearClassifierMixin):

    @property
    def metric(self):
        return BinaryMetrics

class PartialLinearMulticlassMixin(PartialLinearClassifierMixin):

    @property
    def metric(self):
        return MulticlassMetrics

class PartialANNBinaryMixin(PartiallANNClassifierMixin):

    @property
    def metric(self):
        return BinaryMetrics

class PartialANNMulticlassMixin(PartiallANNClassifierMixin):

    @property
    def metric(self):
        return MulticlassMetrics

class PartialXGBoostBinaryMixin(PartialXGBoost):

    def _setup(self, config):
        super()._setup(config)
        self.model = self.model_signature(**{**self.model.get_xgb_params(),
                                             **self.model_parameters,
                                             **self.training_objective})

    @property
    def model_signature(self):
        return XGBClassifier

    @property
    def training_objective(self):
        return {"objective": "binary:logistic"}

    @property
    def metric(self):
        return MulticlassMetrics

    @property
    def get_current_loss(self):
        return float(self.model.evals_result()["validation_0"]["logloss"].pop())

    @property
    def fit_method_parameters(self):
        return {"eval_set": [(self.X_train, self.Y_train)],
                "eval_metric": "logloss"}


    def evaulate_model(self, tracking_metrics):

        metrics = {}
        log_loss = None

        if "log_loss" in tracking_metrics:
            tracking_metrics.remove("log_loss")
            log_loss=True

        metrics.update(super().evaulate_model(tracking_metrics))

        if log_loss:
            if hasattr(self, "X_validation"):
                metrics.update({"validation_" + "log_loss": self.metric.get_metric_score(self.Y_validation,
                                                                                         self.model.predict_proba(
                                                                                             self.X_validation),
                                                                                         "log_loss")})

            metrics.update({"test_" + "log_loss": self.metric.get_metric_score(self.Y_test,
                                                                               self.model.predict_proba(self.X_test),
                                                                                   "log_loss")})
            tracking_metrics.append("log_loss")
        return metrics

class PartialXGBoostMulticlassMixin(PartialXGBoost):

    def _setup(self, config):
        super()._setup(config)
        self.classes = unique(self.Y_train)
        self.model = self.model_signature(**{"num_class": len(self.classes),
                                             **{**self.model.get_xgb_params(),
                                                **self.model_parameters,
                                                **self.training_objective}})

    @property
    def model_signature(self):
        return XGBClassifier

    @property
    def training_objective(self):
        return {"objective": "multi:softmax"}

    @property
    def metric(self):
        return MulticlassMetrics

    @property
    def get_current_loss(self):
        return float(self.model.evals_result()["validation_0"]["mlogloss"].pop())

    @property
    def fit_method_parameters(self):
        return {"eval_set": [(self.X_train, self.Y_train)],
                "eval_metric": "mlogloss"}


class PartialXGBoostRandomForestBinaryMixin(PartialXGBoostBinaryMixin):

    @property
    def model_signature(self):
        return XGBRFClassifier


class PartialXGBoostRandomForestMulticlassMixin(PartialXGBoostMulticlassMixin):

    @property
    def model_signature(self):
        return XGBRFClassifier

class PartialLightGBMBinaryMixin(PartialLightGBM):

    @property
    def metric(self):
        return BinaryMetrics

    @property
    def training_objective(self):
        return {"objective": "binary",
                "metric": "binary_logloss"}

    @property
    def get_current_loss(self):
        return float(list(self.eval_results["train"].values()).pop().pop())


class PartialLightGBMMulticlassMixin(PartialLightGBM):

    def _setup(self, config):
        super()._setup(config)
        self.model_params.update({"num_class": len(unique(self.Y_train))})

    @property
    def metric(self):
        return MulticlassMetrics

    @property
    def training_objective(self):
        return {"objective": "multiclass",
                "metric": "softmax"}

    @property
    def get_current_loss(self):
        return float(list(self.eval_results["train"].values()).pop().pop())