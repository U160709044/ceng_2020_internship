from functools import reduce
from operator import add

from b2m.ml.b2optimization.models.model_base import *
from b2m.ml.b2optimization.optimization_base import PartialParameterTuner

import ray
from lightgbm import Dataset, train

class PartialLinearModel(PartialParameterTuner):

    @property
    def get_current_loss(self):
        return reduce(add, self.skout) / len(self.skout)

class PartialXGBoost(PartialParameterTuner):
    
    def train_model(self):
        if self.booster:
            booster = self.model.fit(self.X_train, self.Y_train, 
                                     xgb_model=self.booster,
                                     **self.fit_method_parameters)
            self.booster = booster.get_booster()
        else:
            booster = self.model.fit(self.X_train, self.Y_train,
                                     **self.fit_method_parameters)
            self.booster = booster.get_booster()
        progress_metrics = self.evaulate_model(self.tracking_metrics)
        progress_metrics.update({"train_loss": self.get_current_loss})

        return progress_metrics

class PartialXGBoostModel(PartialXGBoost, XGBBoosterModel):
    pass

class PartialXGBoostRandomForestModel(XGBBoostedRandomForestModel, PartialXGBoost):
    pass

class PartialANNModel(ANNModel, PartialParameterTuner):
    pass

class PartialLightGBM(PartialParameterTuner):

    def _setup(self, config):
        from b2m.ml.pipeline import B2MPipeline
        self.eval_results = {}
        self.model_params = {**self.model_parameters,
                             **self.training_objective,}
        self.model_params.update(**{key: val for key, val in config.items() \
                                    if not isinstance(val, dict)})
        self.model_path = None
        self.booster = None
        self.trainer_metric = config["experiment"]["train_scorer"]
        self.tracking_metrics = config["experiment"]["evaulation_scorers"]

        pipeline = B2MPipeline()
        x,y = list(config["data"].values())

        if "validation_split_ratio" in config["experiment"]:
            (X_train, Y_train), (X_test, Y_test), (X_validation, Y_validation) = \
                                     pipeline(x, y,
                                             test_split_ratio=config["experiment"]["test_split_ratio"],
                                             validation_split_ratio=config["experiment"]["validation_split_ratio"])

            self.X_train, self.Y_train = ray.get([X_train, Y_train])
            self.X_test, self.Y_test = ray.get([X_test, Y_test])
            self.X_validation, self.Y_validation = ray.get([X_validation, Y_validation])
            self.dataset = Dataset(data=self.X_train, label=self.Y_train, free_raw_data=False)


        else:
            (X_train, Y_train), (X_test, Y_test) = pipeline(x, y,
                                                            test_split_ratio=config["experiment"]\
                                                                                   ["test_split_ratio"])

            self.cross_validation_criteria = config["experiment"]["cross_validation_criteria"]["cv"]
            self.X_train, self.Y_train = ray.get([X_train, Y_train])
            self.X_test, self.Y_test = ray.get([X_test, Y_test])
            self.dataset = Dataset(data=self.X_train, label=self.Y_train, free_raw_data=False)


    def train_model(self):
        if self.booster:
            self.booster = train(params=self.model_params,
                                 **self.fit_method_parameters,
                                 init_model=self.booster,)
        else:
            self.booster = train(params=self.model_params,
                                 **self.fit_method_parameters)

        progress_metrics = self.evaulate_model(self.tracking_metrics)
        progress_metrics.update({"train_loss": self.get_current_loss})
        return progress_metrics


class PartialSVMModel(PartialLinearModel):

    @property
    def model_parameters(self):
        return {"verbose":1,
                "loss": "epsilon_insensitive",
                "learning_rate": "invscaling",}

    def _setup(self, config):
        super()._setup(config)
        #TODO MAKE THIS PIECE DYNAMIC WITH B2 PIPELINE
        from sklearn.kernel_approximation import Nystroem
        nyst = Nystroem(gamma=0.2, n_components=100)
        nyst.fit(self.X_train)
        self.X_train = nyst.transform(self.X_train)