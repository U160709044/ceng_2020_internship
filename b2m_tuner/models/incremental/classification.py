from b2m.ml.b2optimization.models.classifier_mixins import *
from b2m.ml.b2optimization.models.model_io import *
from b2m.ml.b2optimization.models.incremental.incremental_model_base import *


class LogisticBinaryClassifier(PickleModelIO, LinearModel, PartialLinearBinaryMixin):
    pass

class LogisticMulticlassClassifier(PickleModelIO, LinearModel, PartialLinearMulticlassMixin):
    pass

class LassoBinarClassifier(PickleModelIO, LassoClassificationModel, PartialLinearBinaryMixin):
    pass

class LassoMulticlassClassifier(PickleModelIO, LassoClassificationModel, PartialLinearMulticlassMixin):
    pass

class ElasticNetBinarClassifier(PickleModelIO, ElasticClassificationModel, PartialLinearBinaryMixin):
    pass

class ElasticNetMulticlassClassifier(PickleModelIO, ElasticClassificationModel, PartialLinearMulticlassMixin):
    pass

class ANNBinaryClassifier(PickleModelIO, ANNModel, PartialANNBinaryMixin):
    pass

class ANNMulticlassClassifier(PickleModelIO, ANNModel, PartialANNMulticlassMixin):
    pass

class XGBoostGBTreeBinaryClassifier(BoosterModelIO, XGBBoosterModel, PartialXGBoostBinaryMixin):
    pass

class XGBoostGBDartBinaryClassifier(BoosterModelIO, XGBoostDartModel, PartialXGBoostBinaryMixin):
    pass

class XGBoostGBLinearBinaryClassifier(BoosterModelIO, XGBoostLinearModel, PartialXGBoostBinaryMixin):
    pass

class XGBoostGBTreeMulticlassClassifier(BoosterModelIO, XGBBoosterModel, PartialXGBoostMulticlassMixin):
    pass

class XGBoostGBDartMulticlassClassifier(BoosterModelIO, XGBoostDartModel, PartialXGBoostMulticlassMixin):
    pass

class XGBoostGBLinearMulticlassClassifier(BoosterModelIO, XGBoostLinearModel, PartialXGBoostMulticlassMixin):
    pass

class XGBoostGBTreeRandomForestBinaryClassifier(BoosterModelIO, XGBBoostedRandomForestModel, PartialXGBoostRandomForestBinaryMixin):
    pass

class XGBoostGBDartRandomForestBinaryClassifier(BoosterModelIO, XGBoostedDartRandomForestModel, PartialXGBoostRandomForestBinaryMixin):
    pass

class XGBoostGBLinearRandomForestBinaryClassifier(BoosterModelIO, XGBoostLinearModel, PartialXGBoostRandomForestBinaryMixin):
    pass

class XGBoostGBTreeRandomForestMulticlassClassifier(BoosterModelIO, XGBBoostedRandomForestModel, PartialXGBoostRandomForestMulticlassMixin):
    pass

class XGBoostGBDartRandomForestMulticlassClassifier(BoosterModelIO, XGBoostedDartRandomForestModel, PartialXGBoostRandomForestMulticlassMixin):
    pass

class XGBoostGBLinearRandomForestMulticlassClassifier(BoosterModelIO, XGBoostedLinearRandomForestModel, PartialXGBoostRandomForestMulticlassMixin):
    pass

class LightgbmGBTreeBinaryClassifier(LigthGBMBoosterModelIO, LightGBMModel, PartialLightGBMBinaryMixin):
    pass

class LightgbmGBDartBinaryClassifier(LigthGBMBoosterModelIO, LightGBMDart, PartialLightGBMBinaryMixin):
    pass

class LightgbmGBOssBinaryClassifier(LigthGBMBoosterModelIO, LightGBMOneSideSampler, PartialLightGBMBinaryMixin):
    pass

class LightgbmGBTreeMulticlassClassifier(LigthGBMBoosterModelIO, LightGBMModel, PartialLightGBMMulticlassMixin):
    pass

class LightgbmGBDartMulticlassClassifier(LigthGBMBoosterModelIO, LightGBMDart, PartialLightGBMMulticlassMixin):
    pass

class LightgbmGBOssMulticlassClassifier(LigthGBMBoosterModelIO, LightGBMOneSideSampler, PartialLightGBMMulticlassMixin):
    pass

