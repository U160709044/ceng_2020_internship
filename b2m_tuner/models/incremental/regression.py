from b2m.ml.b2optimization.models.model_io import *
from b2m.ml.b2optimization.models.incremental.incremental_model_base import *

from sklearn.linear_model import SGDRegressor
from sklearn.neural_network import MLPRegressor
from xgboost import XGBRegressor
from xgboost import XGBRFRegressor

class LinearRegressor(PickleModelIO, PartialLinearModel):

    @property
    def model_signature(self):
        return SGDRegressor

    @property
    def model_parameters(self):
        return {"verbose":1,
                "loss": "squared_loss", #TODO COPY AND ADD LOSS
                "learning_rate": "invscaling",
                "penalty": "l2"}

    def _setup(self, config):
        super(LinearRegressor, self)._setup(config)
        self.MLType = "Regression"

class LassoRegressor(LinearRegressor, PickleModelIO):

    @property
    def model_parameters(self):
        return {"verbose":1,
                "loss": "squared_loss",
                "learning_rate": "invscaling",
                "penalty": "l1"} #TODO COPY AND ADD LOSS AND PENALTY

    @classmethod
    def learning_space_distributions(cls):
        return {"eta0": hp.loguniform("eta0", -4, -1),
                "power_t": hp.uniform("power_t", 0.2, 0.6),
                "alpha": hp.loguniform("alpha", -6, -2),}


class ElasticNetRegressor(LinearRegressor, PickleModelIO):

    @property
    def model_parameters(self):
        return {"verbose":1,
                "loss": "squared_loss",
                "learning_rate": "invscaling",
                "penalty": "elasticnet"}

    @classmethod
    def learning_space_distributions(cls):
        return {"eta0": hp.loguniform("eta0", -4, -1),
                "power_t": hp.uniform("power_t", 0.2, 0.6),
                "alpha": hp.loguniform("alpha", -6, -2),
                "l1_ratio": hp.uniform("l1_ratio", 0.2, 1)} # ADD L1

class XGBoostRegressor(BoosterModelIO, PartialXGBoostModel): #BoosterModelIO
    
    @property
    def model_signature(self):
        return XGBRegressor
    
    @property
    def model_parameters(self):
        model_params = super().model_parameters.copy()
        model_params.update({"objective": "reg:squarederror"})
        return model_params

    @property
    def fit_method_parameters(self):
        return {"eval_set": [(self.X_train, self.Y_train)],
                "eval_metric": "rmse"}

    @property
    def get_current_loss(self):
        return float(self.model.evals_result()["validation_0"]["rmse"].pop())


class XGBoostRandomForestRegressor(BoosterModelIO, PartialXGBoostRandomForestModel):

    @property
    def model_signature(self):
        return XGBRFRegressor

    @property
    def fit_method_parameters(self):
        return {"eval_set": [(self.X_train, self.Y_train)],
                "eval_metric": "rmse"}

    @property
    def get_current_loss(self):
        return float(self.model.evals_result()["validation_0"]["rmse"].pop())

class LightGBMRegressor(LigthGBMBoosterModelIO, PartialLightGBMModel):

    @property
    def model_signature(self):
        return {}

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                "metric": "rmse"}

class LightGBMRegressorL1(LightGBMRegressor):

    @property
    def model_parameters(self):
        return {**super().model_parameters,
                **{"objective": "regression_l1",
                   "reg_alpha": hp.loguniform("req_lambda", -3, 0.5)}}

"""
Not yet supported.
class LightGBMRandomForestRegressor(LightGBMRegressor):

    @property
    def model_parameters(self):
        model_params = super().model_parameters.copy()
        model_params.update({"boosting": "rf"})
        return model_params

"""
class ANNRegressor(PickleModelIO, PartialANNModel):

    @property
    def model_signature(self):
        return MLPRegressor

    def _setup(self, config):
        super(ANNRegressor, self)._setup(config)
        self.MLType = "Regression"

#TODO INCREMENTAL SVM
class SVMRegressor(PickleModelIO, PartialSVMModel):
    
    @property
    def model_signature(self):
        return SGDRegressor

    def _setup(self, config):
        super(SVMRegressor, self)._setup(config)
        self.MLType = "Regression"
