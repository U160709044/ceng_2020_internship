"""
Seq2Seq time series implementation, should be adapted for b2metric, should run on well-known benchmarks.

import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SequentialSampler, BatchSampler

torch.set_default_dtype(torch.float64)

import numpy as np
import pandas as pd
data = pd.read_csv("/content/drive/My Drive/Google Colab Research/jena_climate_2009_2016.csv")
univariate_time_series = data["T (degC)"]
univariate_time_series.index = data['Date Time']
TRAIN_SPLIT_SIZE = (365 * 144) * 2
X_train, X_test = univariate_time_series[:TRAIN_SPLIT_SIZE], \
                  univariate_time_series[TRAIN_SPLIT_SIZE:TRAIN_SPLIT_SIZE+(365 * 144 * 1)]
mean, std = np.mean(X_train), np.std(X_train)
X_train = (X_train.values - mean) / std
X_test = (X_test.values - mean) / std

class TimeSeriesIterator(Dataset):

    def __init__(self, X, history_window_slice, future_window_slice,
                 starting_timestamp_index = 0, ending_timestamp_index = None):
      self.source = X
      self.history_slice = history_window_slice
      self.future_slice = future_window_slice
      self.start_timestamp = starting_timestamp_index
      self.ending_timestamp = ending_timestamp_index

    def __len__(self):
      return len(self.X)

    def __getitem__(self, idx):
      series_input = self.source[idx:idx+self.history_slice]
      prediction_targets = self.source[idx+self.history_slice:idx+self.history_slice+self.future_slice]
      return series_input[..., np.newaxis], prediction_targets.T[..., np.newaxis]

#TODO MULTIVARIATE
def time_series_slider(data, history_window_slice, future_windows_slice,
                       starting_timestamp_index = 0, ending_timestamp_index = None):
    history_window_features = []
    target_features = []
    if ending_timestamp_index is None:
        ending_timestamp_index = len(data) - (history_window_slice + future_windows_slice)
    for index in range(starting_timestamp_index, ending_timestamp_index):
        history_window_features.append([data[index:index+history_window_slice]])
        target_features.append(data[index+history_window_slice:index+history_window_slice+future_windows_slice])
    history_window_features = np.concatenate(history_window_features,0)
    target_features = np.concatenate(target_features).reshape(-1, future_windows_slice)
    history_window_features = (history_window_features - np.mean(history_window_features)) / np.std(history_window_features)
    target_features = (target_features - np.mean(target_features)) / np.std(target_features)
    return history_window_features[..., np.newaxis], target_features[..., np.newaxis]


class TimeSeriesSeq2Seq(nn.Module):

    def __init__(self, encoder_num_layer, hidden_size, dropouts, in_sequence_length, in_feature_size,
                 decoder_num_layer, out_sequence_length):
        super().__init__()
        self.loss_history, self.validation_history = [], []
        self.in_sequence_length = in_sequence_length
        self.out_sequence_length = out_sequence_length
        self.left_rnn = nn.GRU(input_size=in_feature_size, hidden_size=hidden_size, batch_first=True,
                               num_layers=encoder_num_layer, dropout=dropouts[0])
        self.right_rnn = nn.GRU(input_size=1, hidden_size=hidden_size, batch_first=True, num_layers=decoder_num_layer,
                                dropout=dropouts[1])
        self.fc = nn.Linear(hidden_size, in_feature_size)

    def forward(self, in_time_series, previous_output):
        _, encoder_hidden_state = self.left_rnn(in_time_series)
        decoder_outputs = []
        previous_output, decoder_hidden_state = self.right_rnn(previous_output, encoder_hidden_state)
        previous_output = self.fc(previous_output)
        decoder_outputs.append(previous_output)
        for _ in range(self.out_sequence_length - 1):
            previous_output, decoder_hidden_state = self.right_rnn(previous_output, decoder_hidden_state)
            previous_output = self.fc(previous_output)
            decoder_outputs.append(previous_output)
        return torch.stack(decoder_outputs).squeeze()

    def _train(self, X_train, X_test, batch_size, epoch_num, ):
        self.cuda()
        train_sampler = BatchSampler(SequentialSampler(list(range(len(X_train) - self.in_sequence_length + 1))),
                                     batch_size=batch_size, drop_last=False)
        test_sampler = BatchSampler(SequentialSampler(list(range(len(X_test) - self.in_sequence_length + 1))),
                                    batch_size=batch_size, drop_last=False)
        X_train = TimeSeriesIterator(X_train, self.in_sequence_length, self.out_sequence_length)
        X_test = TimeSeriesIterator(X_test, self.in_sequence_length, self.out_sequence_length)
        data_iter = DataLoader(X_train, batch_sampler=train_sampler)
        validation_set_iter = DataLoader(X_test, batch_sampler=test_sampler)
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-2, weight_decay=1e-5)
        criterion = torch.nn.MSELoss()
        for _ in range(epoch_num):
            train_loss = []
            model = self.train()
            for inputs, labels in data_iter:
                optimizer.zero_grad()
                predictions = model(inputs.cuda(), torch.cat([i[0][-1].view(1, 1, 1) for i in inputs]).cuda())
                loss = criterion(labels.T.squeeze().cuda(), predictions).cuda()
                loss.backward()
                print(loss.item())
                optimizer.step()
            with torch.no_grad():
                model = self.eval()
                for val_inputs, val_labels in validation_set_iter:
                    predictions = model(val_inputs.cuda(), torch.cat([i[0][-1].view(1, 1, 1) for i in inputs]).cuda())
                    val_loss = criterion(val_labels.T.squeeze(), predictions)
                    pint(val_loss.item())
        return model

model = TimeSeriesSeq2Seq(2,256,[0.3,0.3],720,1,2,5)
model._train(X_train, X_test, 32, 1)
"""