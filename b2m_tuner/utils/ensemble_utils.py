from abc import ABC, abstractmethod

class EnsembleBase(ABC):

    @property
    @abstractmethod
    def get_ensemble_models(self):
        pass

    @abstractmethod
    def predict(self, X_test):
        raise NotImplementedError

    @property
    @abstractmethod
    def metric_class(self):
        raise NotImplementedError

    @abstractmethod
    def serialize_ensemble_models(self, output_directory):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def evaulate_ensemble_models(X_test, Y_test, ensembles_models, metric_class, metric):
        return [metric_class.get_metric_score(Y_test,
                                              model.predict(X_test),
                                              metric) for model in ensembles_models]

class RegressorEnsemble:
    pass

class BinaryEnsemble:
    pass

class MulticlassEnsemble:
    pass