import numpy as np

def reduce_pandas_memory_usage(df):
    schema = {}
    initial_memory_size = df.memory_usage().sum() / 1024 ** 2
    uint_boundaries = [np.iinfo("uint8"), np.iinfo("uint16"), np.iinfo("uint32")]
    int_boundaries = [np.iinfo("int8"), np.iinfo("int16"), np.iinfo("int32")]
    float_boundaries = [np.finfo("float16"), np.iinfo("float32"), np.iinfo("float64")]

    for column in initial_memory_size.columns:
        if df[column].dtype != object:  # Exclude strings

            is_integer = False
            maximum_cell_value = df[column].max()
            minimum_cell_value = df[column].min()

            # test if column can be converted to an integer
            asint = df[column].astype(np.int64)
            result = (df[column] - asint)
            result = result.sum()
            if result > -0.01 and result < 0.01:
                is_integer = True

            if is_integer:
                if minimum_cell_value >= uint_boundaries[0].min:
                    for uint_boundary in uint_boundaries:
                        if maximum_cell_value < uint_boundary.max:
                            df[column] = df[column].astype(uint_boundary.dtype)
                    else:
                        df[column] = df[column].astype(np.uint64)
                else:
                    for int_boundary in int_boundaries:
                        if minimum_cell_value > int_boundary.min and maximum_cell_value < int_boundary.max:
                            df[column] = df[column].astype(int_boundary.dtype)
                    else:
                        df[column] = df[column].astype(np.int64)

            else:
                df[column] = df[column].astype(np.float32)

    return df