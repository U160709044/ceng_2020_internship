from abc import ABC, abstractmethod

import numpy as np
from numpy.random import permutation, multinomial

class IndexSampler(ABC):

    def __init__(self, index_upperbound):
        self.upper_bound = index_upperbound

    @abstractmethod
    def __iter__(self):
        raise NotImplementedError

class SequentialIndexSampler(IndexSampler):

    def __init__(self, index_upperbound):
        super(SequentialIndexSampler, self).__init__(index_upperbound)

    def __iter__(self):
        return iter(range(self.upper_bound))

    def __len__(self):
        return self.upper_bound

class PermutedIndexSampler(IndexSampler):

    def __init__(self, index_upperbound):
        super(PermutedIndexSampler, self).__init__(index_upperbound)

    def __iter__(self):
        return iter(permutation(np.arange(self.upper_bound)))

    def __len__(self):
        return self.upper_bound

class WeightedIndexSampler(IndexSampler):

    def __init__(self, index_upperbound, weights):
        super(WeightedIndexSampler, self).__init__(index_upperbound)
        self.weights = weights

    def __iter__(self):
        indices = multinomial(5, self.weights)
        return iter(indices)

    def __len__(self):
        return self.upper_bound

class BatchIndicesSampler(IndexSampler):

    def __init__(self, sampler_type, batch_size, include_last_batch=False):

        self.sampler = sampler_type
        self.batch_size = batch_size
        self.include_last_batch = include_last_batch

    def __iter__(self):
        batch = []
        for index in self.sampler:
            batch.append(index)
            if len(batch) == self.batch_size:
                yield batch
                batch = []
        if len(batch) > 0 and not self.include_last_batch:
            yield batch

    def __len__(self):
        if self.include_last_batch:
            return len(self.sampler) // self.batch_size
        else:
            return (len(self.sampler) + self.batch_size - 1) // self.batch_size