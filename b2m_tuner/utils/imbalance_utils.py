from b2m.ml.b2optimization.optimization_base import TransformerBase

from hyperopt import hp
from imblearn.combine import SMOTETomek
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import CondensedNearestNeighbour, OneSidedSelection, RandomUnderSampler


class CondensedNNUnderSampler(TransformerBase):

    def model_signature(self):
        return CondensedNearestNeighbour

    @staticmethod
    def learning_space_distributions():
        return {"n_neighbors": hp.choice("n_neighbors", [1,2,3]),}

    def transform(self, X_train, Y_train):
        return self.model.fit_resample(self, X_train, Y_train)

class OneSidedSelectionUnderSampler(TransformerBase):

    def model_signature(self):
        return OneSidedSelection

    @staticmethod
    def learning_space_distributions():
        return {"n_neighbors": hp.choice("n_neighbors", [1,2,3]),}

    def transform(self, X_train, Y_train):
        return self.model.fit_resample(self, X_train, Y_train)

class NearMissV3UnderSampler(TransformerBase):

    def model_signature(self):
        return OneSidedSelection

    def model_parameters(self):
        return {"sampling_strategy": 0.5,}

    @staticmethod
    def learning_space_distributions():
        return {"n_neighbors": hp.choice("n_neighbors", [3,4,5,6]),}

    def transform(self, X_train, Y_train):
        return self.model.fit_resample(self, X_train, Y_train)


class RandomizedUnderSampler(TransformerBase):

    def model_signature(self):
        return RandomUnderSampler

    def model_parameters(self):
        return {"sampling_strategy": 0.5,}

    @staticmethod
    def learning_space_distributions():
        return {}

    def transform(self, X_train, Y_train):
        return self.model.fit_resample(self, X_train, Y_train)

class RandomizedOverSampler(TransformerBase):

    def model_signature(self):
        return RandomOverSampler

    def model_parameters(self):
        return {"sampling_strategy": 0.5,}

    @staticmethod
    def learning_space_distributions():
        return {}

    def transform(self, X_train, Y_train):
        return self.model.fit_resample(self, X_train, Y_train)


class SMOTETomekCombinationSampler(TransformerBase):

    def model_signature(self):
        return SMOTETomek

    def model_parameters(self):
        return {"sampling_strategy": 0.5,}

    @staticmethod
    def learning_space_distributions():
        return {}

    def transform(self, X_train, Y_train):
        return self.model.fit_resample(self, X_train, Y_train)