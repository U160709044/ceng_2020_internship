import sys
import time
from io import StringIO

import numpy as np
from ray.tune import Stopper

class CaptureSklearnReport(list):

    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend([float(line.split("loss: ")[-1]) \
                     for line in self._stringio.getvalue().split('\n') \
                     if not len(line.split("loss: ")) == 1])
        del self._stringio
        sys.stdout = self._stdout

class EarlyValidationStopper(Stopper):

    def __init__(self, metric, patience=7, mode="min"):

        self.patience = patience
        self.mode = mode
        self.metric = metric

        self.trials = {}

    def stop_all(self):
        return False

class TimeStopper(Stopper):

    def __init__(self, allowed_time_bound):
        self._experiment_start_timestamp = time.time()
        self.time_bound = allowed_time_bound

    def __call__(self, trial_id, result):
        return self.stop_all()

    def stop_all(self):
        return (time.time() - self._experiment_start_timestamp) > self.time_bound

class EarlyPlateauedStopper(EarlyValidationStopper):

    def __init__(self, metric, patience=7, std=0.001):
        super(EarlyPlateauedStopper, self).__init__(metric, patience)
        self.std = std
        self.has_plateaued = lambda array : (len(array) == self.patience
                                             and np.std(np.array(array)) <= self.std)

    def __call__(self, trial_id, result):

        if trial_id not in self.trials:
            self.trials[trial_id] = {"trial_scores": [],
                                     "plateaued_counter": 0}
        else:
            self.trials[trial_id]["trial_scores"] = self.trials[trial_id]["trial_scores"][-(self.patience - 1):] \
                                                    + [result[self.metric]]

        print(self.trials[trial_id]["trial_scores"])
        if len(self.trials[trial_id]["trial_scores"]) != 7:
            early_stop = False
        else:
            if self.has_plateaued(self.trials[trial_id]["trial_scores"]):
                self.trials[trial_id]["plateaued_counter"] +=1
                early_stop = False
                if self.trials[trial_id]["plateaued_counter"] == 7:
                    early_stop = True
                    del self.trials[trial_id]
            else:
                early_stop = False
        return early_stop

class EarlyImprovementStopper(EarlyValidationStopper):

    def __init__(self, metric, patience=7, mode="min", tol=0.0001):
        super(EarlyImprovementStopper, self).__init__(metric, patience, mode)
        self.tol = tol

    def __call__(self, trial_id, result):

        if self.mode == "min":
            score = -1.0 * result[self.metric]
        else:
            score = np.copy(result[self.metric])

        if trial_id not in self.trials:
            self.trials[trial_id] = (score, 0)

        if score < self.trials[trial_id][0] + self.tol:
            self.trials[trial_id] = (score, self.trials[trial_id][1] + 1)
        else:
            self.trials[trial_id] = (score, 0)

        if self.trials[trial_id][1] >= self.patience:
            early_stop = True
            del self.trials[trial_id]
        else:
            early_stop = False

        return early_stop

class CombinedStopper(Stopper):

    def __init__(self, improvement_metric, plateaued_metric,
                       improvement_patience=7, plateaued_patience=7,
                       improvement_mode="min", tol=0.0001, std=0.001):

        self.early_improvement_stopper = EarlyImprovementStopper(improvement_metric, improvement_patience,
                                                                 improvement_mode, tol)

        self.early_plateaued_stopper = EarlyPlateauedStopper(plateaued_metric, plateaued_patience,
                                                                 improvement_mode, std)

        self.early_time_stopper = TimeStopper(allowed_time_bound=float("inf"))


    def __call__(self, trial_id, result):

        return any([self.early_improvement_stopper(trial_id, result),
                    self.early_plateaued_stopper(trial_id, result)])

    def stop_all(self):
        return self.early_time_stopper.stop_all()
