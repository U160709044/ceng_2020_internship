from abc import ABC, abstractmethod

import ray
from b2m_tuner.pipeline import B2MPipeline
from ray import tune
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

# TODO MAKE PARAMETER TUNE COMPT FOR REGRES & CLASS
"""    @property
    @abstractmethod
    def model_signature(self):
        return NotImplementedError()
"""
class ModelBase(ABC):

    @property
    def model_parameters(self):
        return {}

    @classmethod
    @abstractmethod
    def learning_space_distributions(cls):
        return NotImplementedError()

    def evaulate_model(self, tracking_metrics):
        metrics = {}
        
        if hasattr(self, "X_validation"):
            metrics.update({"validation_" + metric: self.metric.get_metric_score(self.Y_validation,
                                                            self.model.predict(self.X_validation),
                                                            metric)
                                                            for metric in tracking_metrics})

        metrics.update({"test_" + metric: self.metric.get_metric_score(self.Y_test,
                                                        self.model.predict(self.X_test),
                                                        metric)
                                                        for metric in tracking_metrics})
        return metrics


class ParameterTuner(tune.Trainable):

    def _setup(self, config):
        self.model_params = self.model_parameters
        self.model_params.update(**{key: val for key, val in config.items() \
                               if not isinstance(val, dict)})
        self.model = self.model_signature(**self.model_params)
        self.model_path = None
        self.booster = None
        self.trainer_metric = config["experiment"]["train_scorer"]
        self.tracking_metrics = config["experiment"]["evaulation_scorers"]

    def _train(self):
        return self.train_model()

class PartialParameterTuner(ParameterTuner):

    def train_model(self):
        if self.MLType == "Regression":
            self.model.partial_fit(self.X_train, self.Y_train)
        else:
            self.model.partial_fit(self.X_train, self.Y_train, classes=self.classes)
        progress_metrics = self.evaulate_model(self.tracking_metrics)
        progress_metrics.update({"train_loss":self.get_current_loss})
        return progress_metrics

    def _setup(self, config):
        super()._setup(config)

        pipeline = B2MPipeline()
        x,y = list(config["data"].values())

        if "validation_split_ratio" in config["experiment"]:
            (X_train, Y_train), (X_test, Y_test), (X_validation, Y_validation) = \
                                     pipeline(x, y,
                                             test_split_ratio=config["experiment"]["test_split_ratio"],
                                             validation_split_ratio=config["experiment"]["validation_split_ratio"])

            self.X_train, self.Y_train = ray.get([X_train, Y_train])
            self.X_test, self.Y_test = ray.get([X_test, Y_test])
            self.X_validation, self.Y_validation = ray.get([X_validation, Y_validation])

        else:
            (X_train, Y_train), (X_test, Y_test) = pipeline(x, y,
                                                            test_split_ratio=config["experiment"]\
                                                                                   ["test_split_ratio"])

            self.cross_validation_criteria = config["experiment"]["cross_validation_criteria"]["cv"]
            self.X_train, self.Y_train = ray.get([X_train, Y_train])
            self.X_test, self.Y_test = ray.get([X_test, Y_test])

class BatchParameterTuner(ParameterTuner):

    def train_model(self):
        scores = self.cross_validation_criteria.cross_validate(self.model,
                                                      self.X_train, self.Y_train,
                                                      self.metric, [self.cv_metric])
        return scores
    
    def cross_validate(self):
        scores = self.cross_validation_criteria.cross_validate(self.model,
                                                      self.X_train, self.Y_train,
                                                      self.metric, [self.cv_metric])
        

    def _setup(self, config):
        super()._setup(config)

        self.cross_validation_criteria = config["experiment"]["cross_validation_criteria"]["cv"]
        self.cv_metric = config["experiment"]["cross_validation_criteria"]["metric"]

        pipeline = B2MPipeline()
        x,y = list(config["data"].values())


        (X_train, Y_train), (X_test, Y_test) = pipeline(x, y,
                                                        test_split_ratio=config["experiment"]["test_split_ratio"])

        self.cross_validation_criteria = config["experiment"]["cross_validation_criteria"]["cv"]
        self.X_train, self.Y_train = ray.get([X_train, Y_train])
        self.X_test, self.Y_test = ray.get([X_test, Y_test])
