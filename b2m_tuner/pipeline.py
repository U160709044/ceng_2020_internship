from b2m_tuner.preprocessing.outlier_inspector import LocalOutlierFactorInspector
from b2m_tuner.optimization_base import *

from sklearn.preprocessing import RobustScaler
from sklearn.model_selection import train_test_split

import ray
import numpy as np

class B2MPipeline:

    def __init__(self, pipes=None, pipe_parameters=None):

        self._scaler = RobustScaler()
        self._outlier_remover = LocalOutlierFactorInspector()
        """
        for pipe in pipes:
            if isinstance(pipe, OutlierDetectionBase):
                self._outlier_remover = pipe
            if isinstance(pipe, ImputerBase):
                self._imputer = pipe
            if isinstance(pipe, ScalerBase):
                self._scaler = pipe
            if isinstance(pipe, ModelBase):
                self._estimator = pipe
        """

    def __call__(self, X_train, Y_train,
                 test_split_ratio=0.2, validation_split_ratio=None,
                 missing_value=np.NaN):

        return self.prepare_training_set(X_train, Y_train,
                                         test_split_ratio,
                                         validation_split_ratio,
                                         missing_value)

    def prepare_training_set(self, X_train, Y_train,
                             test_split_ratio=0.2,
                             validation_split_ratio=None,
                             missing_value=np.NaN):
        X_train, Y_train = ray.get([X_train, Y_train])
        X_train, X_test, Y_train, Y_test = train_test_split(X_train, Y_train,
                                                            test_size=test_split_ratio)
        self._scaler.fit(X_train)
        X_train = self._scaler.transform(X_train)
        X_train, Y_train = self._outlier_remover.transform(X_train, Y_train)
        """X_train = self._imputer.transform(X_train, missing_value)"""
        X_test = self.preprocess_data(X_test)

        X_test, Y_test = ray.put(X_test, weakref=True), ray.put(Y_test, weakref=True)

        if validation_split_ratio:
            X_train, X_validation, Y_train, Y_validation = train_test_split(X_train, Y_train,
                                                                            test_size=validation_split_ratio)
            X_validation = self.preprocess_data(X_validation)
            X_validation, Y_validation = ray.put(X_validation, weakref=True), ray.put(Y_validation, weakref=True)
            X_train, Y_train = ray.put(X_train, weakref=True), ray.put(Y_train, weakref=True)
            return [(X_train, Y_train), (X_test, Y_test), (X_validation, Y_validation)]

        X_train, Y_train = ray.put(X_train), ray.put(Y_train)

        return [(X_train, Y_train), (X_test, Y_test)]

    def preprocess_data(self, X):
        """self._imputer.model.transform(X)"""
        """self._scaler.model.transform(X)"""
        self._scaler.transform(X)
        return X

    def predict(self, X_test):
        #Feature transformation goes here.
        return self.model.predict(self.preprocess_data(X_test))