import os
import uuid
from operator import itemgetter

import ray
from ray import tune
from ray.tune.schedulers import ASHAScheduler
from ray.tune.suggest.hyperopt import HyperOptSearch
from functools import partial

class ExperimentBase:

    PARAMS = ["local_dir", "stop", "verbose", "resources_per_trial", "name",
              "checkpoint_freq", "checkpoint_at_end"] #"checkpoint_at_end",

    PARAMETER_SUGGESTER = HyperOptSearch

    def __init__(self, log_directory=os.path.join(os.path.expanduser("~"), "tune_logs"),
                 verbosity=0, checkpoint_frequency=10, checkpoint_at_end=True,
                 experiment_stopper=None, experiment_resources=None, experiment_name=None):

        self.analysis = None
        if experiment_stopper is None:  #TODO STOPPER
            experiment_stopper = dict({"training_iteration": 100})

        if experiment_resources is None:
            experiment_resources = dict({"cpu": 1, "gpu": 0})

        if experiment_name is None:
            experiment_name = str(uuid.uuid4())

        self.experiment_configurations = dict(zip(self.__class__.PARAMS,
                                                  [log_directory, experiment_stopper, verbosity,
                                                   experiment_resources, experiment_name,
                                                   checkpoint_frequency, checkpoint_at_end]))


    def __call__(self, X, Y, experiment_class,
                        loss_metric, evaulation_metrics,
                        trial_count=1000, search_algorithm=None, scheduler=None,
                        model_params=None, test_split_ratio=0.2, validation_split_ratio=0.1):

        self.metrics = loss_metric

        data_object_ID = ray.put(X)
        target_object_ID = ray.put(Y)
        del X,Y

        self.experiment_configurations.update({"run_or_experiment": experiment_class,
                                               "num_samples": trial_count,
                                               "config": {"experiment": {**{"train_scorer": [self.metrics["metric"]]},
                                                                        **{"evaulation_scorers": evaulation_metrics},
                                                                        "test_split_ratio": test_split_ratio,
                                                                        "validation_split_ratio": validation_split_ratio},
                                               "data": {"data": data_object_ID,
                                                      "target": target_object_ID}}})
        if search_algorithm:
            self.experiment_configurations.update(dict(search_alg=search_algorithm))
        else:
            self.experiment_configurations.update(dict(search_alg=self.__class__.PARAMETER_SUGGESTER(experiment_class.learning_space_distributions(),
                                                                     **self.metrics)))
        if model_params:
            self.experiment_configurations["config"].update(**model_params)

        self.hyperparams = experiment_class.learning_space_distributions().keys()

        return self

    def get_best_hyperparameters(self, metric=None):
        if metric:
            return {key: val for key, val in self.analysis.get_best_config(metric=metric,
                                                                           mode="min").items() \
                    if key in self.hyperparams}
        else:
            return {key: val for key, val in self.analysis.get_best_config(metric=metric,
                                                                           mode="min").items() \
                    if key in self.hyperparams}

    def get_best_model(self, metric, mode):
        #MAKE THIS CODE PIECE DYNAMIC FOR METRICS
        best_trial = self.analysis.get_best_trial(**{"metric":metric, "mode":mode})
        model_path, model_score = sorted(self.analysis.get_trial_checkpoints_paths(best_trial,
                                                                                   metric=metric),
                      key=itemgetter(1))[-1]
        return model_path, model_score

    def get_best_n_model(self, metric, mode, n=2):

        metric = {"metric":metric, "mode":mode}
        trials = self.analysis.dataframe(**metric)\
                 .sort_values(metric["metric"], ascending=False).iloc[0:n, :]

        params = [column for column in trials.columns \
                 if column.startswith("config") and column.split("/")[-1] in self.hyperparams]

        model_checkpoints = [sorted(self.analysis.get_trial_checkpoints_paths(trial,
                                                                              metric=metric["metric"]), \
                                    key=itemgetter(1))[-1] for trial in trials.loc[:, ["logdir"]].values.squeeze().tolist()]

        hyperparams = trials.loc[:, params]
        hyperparams = [dict(zip([param.rsplit("/")[-1] for param in params],
                                parameter_vals.tolist())) for parameter_vals in hyperparams.values]

        return [dict(model_path=path,score=score,parameter_configuration=params) \
                for (path,score), params in zip(model_checkpoints, hyperparams)]

class IncrementalTuningExperiment(ExperimentBase):

    SCHEDULER = partial(ASHAScheduler, max_t=10, grace_period=5, reduction_factor=2)

    def __init__(self, experiment_stopper=None, exp_name=None, exp_resources=None):
        super(IncrementalTuningExperiment, self).__init__(experiment_stopper=experiment_stopper)

    def __call__(self, X, Y, experiment_class,
                        loss_metric, evaulation_metrics,
                        max_iteration = 10, min_iteration = 2, reduction_factor = 2,
                        trial_count=50, search_algorithm=None, scheduler=None,
                        model_params=None, test_split_ratio=0.2, validation_split_ratio=0.1):

        super().__call__(**{key: item for key, item in locals().items() \
                            if key not in ["early_stopping_metric",
                                           "scheduler", "max_iteration", "min_iteration",
                                           "reduction_factor", "self", "__class__"]})

        if scheduler:
            self.experiment_configurations.update(dict(scheduler=scheduler))
        else:
            param = {"max_t": max_iteration,
                     "grace_period": min_iteration,
                     "reduction_factor": reduction_factor,
                     **self.metrics}
            self.experiment_configurations.update(dict(scheduler=ASHAScheduler(**param)))

        self.analysis = tune.run(**self.experiment_configurations)

        return self

class BatchTuningExperiment(ExperimentBase):

    def __init__(self, experiment_stopper=None, exp_name=None, exp_resources=None):
        super(BatchTuningExperiment, self).__init__(experiment_stopper=experiment_stopper)

    def __call__(self, X, Y, experiment_class,
                        loss_metric, evaulation_metrics,
                        cv_metric, cv_type,


                        trial_count=1000, search_algorithm=None,
                        model_params=None, test_split_ratio=0.2, validation_split_ratio=0.1):

        super().__call__(**{key: item for key, item in locals().items() \
                            if key not in ["cv_type", "cv_metric",
                                           "self", "__class__"]})

        self.experiment_configurations["config"] \
                                      ["experiment"] \
                                      ["cross_validation_criteria"] = {"cv": cv_type,
                                                                       "metric": cv_metric}
        self.analysis = tune.run(**self.experiment_configurations)

        return self