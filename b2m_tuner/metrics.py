from sklearn import metrics
from functools import partial

class RegressionMetrics:
    METRICS = {"explained_variance_score": metrics.explained_variance_score,
               "r2_score": metrics.r2_score,
               "mean_absolute_error": metrics.mean_absolute_error,
               "mean_squared_error": metrics.mean_squared_error,
               "mean_squared_log_error": metrics.mean_squared_log_error,
               "median_absolute_error": metrics.median_absolute_error, }  # warience_weighted extra.}

    @classmethod
    def get_metric_score(cls, y_true, y_pred, metric):
        return cls.METRICS[metric](y_true, y_pred)


class BinaryMetrics:

    METRICS = {"accuracy": metrics.accuracy_score,
               "average_precision_score": metrics.average_precision_score,
               "balanced_accuracy": metrics.balanced_accuracy_score,
               "macro_f1_score": partial(metrics.f1_score, average="macro"),
               "weigthed_f1_score": partial(metrics.f1_score, average="weighted"),
               "log_loss": metrics.log_loss,
               "matthew_correlation_coefficient": metrics.matthews_corrcoef,
               "macro_roc_auc_score": partial(metrics.roc_auc_score, average="macro"),
               "weigthed_roc_auc_score": partial(metrics.roc_auc_score, average="weighted", ),
               "macro_precision_score": partial(metrics.precision_score, average="macro"),
               "weigthed_precision_score": partial(metrics.precision_score, average="weighted"),
               "macro_recall_score": partial(metrics.recall_score, average="macro"),
               "weigthed_recall_score": partial(metrics.recall_score, average="weighted")}

    @classmethod
    def get_metric_score(cls, y_true, y_pred, metric):
        return cls.METRICS[metric](y_true, y_pred)

class MulticlassMetrics:
    METRICS = {"accuracy": metrics.accuracy_score,
               "balanced_accuracy": metrics.balanced_accuracy_score,
               "macro_f1_score": partial(metrics.f1_score, average="macro"),
               "weighted_f1_score": partial(metrics.f1_score, average="weighted"),
               "macro_roc_auc_score": partial(metrics.roc_auc_score, average="macro", multi_class="ovr"),
               "weighted_roc_auc_score": partial(metrics.roc_auc_score, average="weighted", multi_class="ovr"),
               "macro_precision_score": partial(metrics.precision_score, average="macro"),
               "weighted_precision_score": partial(metrics.precision_score, average="weighted"),
               "macro_recall_score": partial(metrics.recall_score, average="macro"),
               "weighted_recall_score": partial(metrics.recall_score, average="weighted")}

    @classmethod
    def get_metric_score(cls, y_true, y_pred, metric):
        return cls.METRICS[metric](y_true, y_pred)
