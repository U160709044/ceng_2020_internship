from b2m_tuner.preprocessing.preprocess_base import CrossValidationBase

from sklearn.model_selection import KFold, ShuffleSplit, StratifiedKFold, \
                                    StratifiedShuffleSplit, GroupKFold, GroupShuffleSplit
from hyperopt import hp

#both for regression & classification, more suitable for regression
class KFoldCV(CrossValidationBase):

    @property
    def model_signature(self):
        return KFold

    @staticmethod
    def learning_space_distributions():
        return {"n_splits": hp.choice("n_splits",
                                              [3,5,7]), }

    def transform(self, *args, **kwargs):
        return {}

#both for regression & classification, more suitable for regression
class ShuffleSplitCV(KFoldCV):

    @property
    def model_signature(self):
        return ShuffleSplit

#for binary, multiclass
class StratifiedKFoldCV(KFoldCV):

    @property
    def model_signature(self):
        return StratifiedKFold

#for binary, multiclass
class StratifiedShuffleSplitCV(KFoldCV):

    @property
    def model_signature(self):
        return StratifiedShuffleSplit

class GroupKFoldCV(KFoldCV):

    @property
    def model_signature(self):
        return GroupKFold

class GroupShuffleSplitCV(KFoldCV):

    @property
    def model_signature(self):
        return GroupShuffleSplit