### Kernel approximations ###
from b2m_tuner.preprocessing.preprocess_base import TransformerBase

from hyperopt import hp
from sklearn.kernel_approximation import Nystroem

class NystroemRBFApproximation(TransformerBase):
    
    @property
    def model_signature(self):
        return Nystroem

    @staticmethod
    def learning_space_distributions():
        return {"gamma": hp.uniform("gamma", 0.1, 1),
                "n_components": hp.choice("n_components", list(range(50,301)))}

    def transform(self, X_train):
        self.model.fit(X_train)
        return self.model.transform(X_train)