from abc import ABC, abstractmethod
from functools import reduce
from operator import add

import numpy as np

class TransformerBase(ABC):

    def __init__(self):
        self.model = self.model_signature(**self.model_parameters)

    @property
    def model_signature(self):
        return NotImplementedError

    @property
    def model_parameters(self):
        return {}

    @staticmethod
    @abstractmethod
    def learning_space_distributions():
        return NotImplementedError

    @abstractmethod
    def transform(self, *args, **kwargs):
        return NotImplementedError

class ImputerBase(TransformerBase):

    def transform(self, X_train, missing_value):
        self.model.fit(X_train)
        return self.model.transform(X_train)

class OutlierDetectionBase(TransformerBase):

    def transform(self, X_train, Y_train):
        self._inliers = np.where(self.model.fit_predict(X_train)!=-1)
        return X_train[self._inliers], Y_train[self._inliers]

class CrossValidationBase(TransformerBase):

    def cross_validate(self, estimator, X_train, Y_train,
                        metric_estimator, metrics, train_cv=False):
        scores = {"test_" + metric: [] for metric in metrics}
        #TODO MAKE THIS PARALLEL ?
        for train_split, test_split in self.model.split(X_train):
            estimator.fit(X_train[train_split], Y_train[train_split])
            preds = estimator.predict(X_train[test_split])
            for metric in metrics:
                scores["test_" + metric].append(metric_estimator.get_metric_score(
                                                Y_train[test_split],
                                                preds,
                                                metric))
        return {scorer: reduce(add, scores[scorer]) / len(scores[scorer]) \
                        for scorer in scores.keys()}
