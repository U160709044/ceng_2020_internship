from b2m_tuner.preprocessing.preprocess_base import ImputerBase

from hyperopt import hp
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import SimpleImputer, KNNImputer, IterativeImputer

class SimpleStrategyImputer(ImputerBase):

    @property
    def model_signature(self):
        return SimpleImputer

    @staticmethod
    def learning_space_distributions():
        return {"imputer_strategy": hp.choice("imputer_strategy",
                                              ["mean","median","most_frequent"]),}

class IterativeImputer(ImputerBase):

    @property
    def model_signature(self):
        return IterativeImputer

    @staticmethod
    def learning_space_distributions():
        return {"max_iter": hp.choice("max_iter", list(range(20,50))),
                "initial_strategy": hp.choice("initial_strategy",["mean","median","most_frequent"]),}

class KNNImputerTransformer(ImputerBase):

    @property
    def model_signature(self):
        return KNNImputer

    @staticmethod
    def learning_space_distributions():
        return {"n_neighbors": hp.uniform("n_neighbors", 0, 0.05),
                "weights": hp.choice("weights", ["uniform","distance"])}