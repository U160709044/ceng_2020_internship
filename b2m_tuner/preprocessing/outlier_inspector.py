from b2m_tuner.preprocessing.preprocess_base import OutlierDetectionBase

from hyperopt import hp
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor

class LocalOutlierFactorInspector(OutlierDetectionBase):

    @property
    def model_signature(self):
        return LocalOutlierFactor

    @staticmethod
    def learning_space_distributions():
        return {"n_neighbors": hp.uniform("nn_neighbors", 0, 0.05),}
        #TODO BallTree & KDTree , monitor performance while leaf_size changes.

class IsolationForestOutlierInspector(OutlierDetectionBase):

    @property
    def model_signature(self):
        return IsolationForest

    @staticmethod
    def learning_space_distributions():
        return {"n_estimators": hp.choice("n_estimators", list(range(75, 125))),
                "max_samples": hp.uniform("max_samples", 0.6, 1),
                "max_features": hp.uniform("max_features", 0.6, 1),
                "boostrap": hp.choice("bootstrap", [True, False]), }